const express = require('express');
const router = express.Router();
var crypto = require("crypto");
 
 function decrypt(text, password){
 
     var m = crypto.createHash('md5');
     m.update(password)
     var key = m.digest('hex');
     // Create iv from password and key
     m = crypto.createHash('md5');
     m.update(password + key)
     var iv = m.digest('hex');
   var input = text.replace(/\-/g, '+').replace(/_/g, '/');
   var edata = new Buffer(input, 'base64').toString('binary');
 
   var decipher = crypto.createDecipher('aes-256-cbc', key, iv.slice(0,16));
 
     var decrypted = decipher.update(edata,'hex','utf8');
     decrypted += decipher.final('utf8');
     var dec = new Buffer(decrypted, 'binary').toString('utf8');
   return dec;
 }
 
 var pass = 'test';

router.post('/', (req, res, next)=>{
    res.status(200).json({
        message: 'POST message'
    })
});

router.get('/:id', (req, res, next)=>{
    var id = req.params.id;
    var dd = decrypt(id, pass);
   
    res.status(200).json({
        message : 'dekripsi data',
        dekripsi : dd
    });
});

router.put('/', (req, res, next)=>{
    if(!req.is('application/json')){
        res.status(404).json({
            message: "DATA HARUS JSON"
        })
    }

    var text = req.body.text;
    try{
        var dd = decrypt(text, pass);
        res.status(200).json({
            message: "ok",
            plaintext: dd
        })    
    }catch(err){
        res.status(404).json({
            message: "data tidak bisa di dekripsi"
        })
    
    }
});

module.exports = router; 