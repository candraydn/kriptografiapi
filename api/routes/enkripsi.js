const express = require('express');
const router = express.Router();
var crypto = require("crypto");

function encrypt(text, password){

    var m = crypto.createHash('md5');
     m.update(password)
     var key = m.digest('hex');
     m = crypto.createHash('md5');
     m.update(password + key)
     var iv = m.digest('hex');
 
     var data = new Buffer(text, 'utf8').toString('binary');
 
     var cipher = crypto.createCipher('aes-256-cbc', key, iv.slice(0,16));
 
     var encrypted = cipher.update(data,'utf8','hex');
     encrypted += cipher.final('hex');
     var crypted = new Buffer(encrypted, 'binary').toString('base64');
 
   return crypted;
 }
 
 
 var pass = 'test';

router.post('/', (req, res, next)=>{
    res.status(200).json({
        message: 'POST message'
    })
});

router.get('/:id', (req, res, next)=>{
    var id = req.params.id;
    var hw = encrypt(id, pass);
   
    res.status(200).json({
        message : 'enkripsi data',
        enkripsi : hw
    });
});

router.put('/', (req, res, next)=>{
    if(!req.is('application/json')){
        res.status(404).json({
            message: "DATA HARUS JSON"
        })
    }

    try{
        var text = req.body.text;
        var hw = encrypt(text, pass);
        res.status(200).json({
            message: "ok",
            plaintext: hw
        })    
    }catch(err){
        res.status(404).json({
            message: "data tidak bisa di enkripsi"
        })
    
    }
});

module.exports = router; 