const express = require("express");
const app = express();
const bodyParser = require("body-parser");

const enkripsiRoute = require('./api/routes/enkripsi');
const dekripsiRoute = require('./api/routes/dekripsi');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use('/enkripsi', enkripsiRoute);
app.use('/dekripsi', dekripsiRoute);

app.use('/', (req, res, next)=>{
    res.json({
        message:'welcome...'
    })
})

module.exports=app;